package com.example.policefir.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.policefir.Model.UserInfoModel;
import com.example.policefir.R;
import com.example.policefir.SharedPreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;

public class SignupActivity extends AppCompatActivity {

    EditText editTextFullName,editTextMobile,editTextEmail,editTextPassword,editTextConfirmPassword;
    Button buttonSignup,buttonCancel;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    ScrollView scrollView;
    //Firebase Variables;
    private FirebaseAuth mAuth;
    FirebaseFirestore mFireStore;

    //Model
    UserInfoModel userInfo;

    SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        sharedPreferenceManager= new SharedPreferenceManager(this);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        linearLayout = (LinearLayout)findViewById(R.id.linearLayout);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        editTextFullName=(EditText)findViewById(R.id.uname);
        editTextMobile=(EditText)findViewById(R.id.phone);
        editTextEmail=(EditText)findViewById(R.id.email);
        editTextPassword=(EditText)findViewById(R.id.password);
        editTextConfirmPassword=(EditText)findViewById(R.id.rePassword);
        buttonSignup=(Button)findViewById(R.id.signup);
        buttonCancel=(Button)findViewById(R.id.cancel);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }


    public void registerUser() {
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String fullName = editTextFullName.getText().toString();
        final String mobile = editTextMobile.getText().toString();
        String confirmPassword = editTextConfirmPassword.getText().toString();


        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (fullName.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextFullName;
            editTextFullName.setError("full name is a required field");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password is a required field");
        }
        if (editTextConfirmPassword.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextConfirmPassword;
            editTextConfirmPassword.setError("password is a required field");
        }
        if (password.length() < 6) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password must be greater than 6 letters");
        }
        if(!password.matches(confirmPassword))
        {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("password does not matches");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is not valid");
        }

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(SignupActivity.this, "Authentication Successfull.", Toast.LENGTH_SHORT).show();
                                String userId = mAuth.getUid();
                                userInfo = new UserInfoModel(fullName,email,password,mobile,userId);
                                mFireStore.collection("UserInfo").document().set(userInfo)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    sharedPreferenceManager.setEmail(email);
                                                    Toast.makeText(SignupActivity.this, "data added", Toast.LENGTH_LONG).show();
                                                    startActivity(new Intent(SignupActivity.this, MainActivity.class));
                                                } else {
                                                    Log.d("ERR",task.getException().toString());
                                                    Toast.makeText(SignupActivity.this, "data not added", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

                            } else {
                                progressBar.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.VISIBLE);
                                scrollView.setVisibility(View.VISIBLE);
                                if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                    Toast.makeText(SignupActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });





        }
    }
}
