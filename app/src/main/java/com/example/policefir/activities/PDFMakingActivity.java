package com.example.policefir.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.pdf.PrintedPdfDocument;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.policefir.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PDFMakingActivity extends AppCompatActivity {

    TextView tvDate,tvName,tvEmail,tvMobile,tvAddress,tvSubject,tvComplaint,tvCompNo;
    Button buttonPrint;
    LinearLayout linearLayout;
    String currentDate,name,emailid,phone,address,subject,complaint,complaint_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfmaking);
        tvDate = (TextView)findViewById(R.id.tvDate);
        tvName = (TextView)findViewById(R.id.tvName);
        tvEmail = (TextView)findViewById(R.id.tvEmail);
        tvMobile = (TextView)findViewById(R.id.tvMobile);
        tvAddress = (TextView)findViewById(R.id.tvAddress);
        tvSubject = (TextView)findViewById(R.id.tvSubject);
        tvComplaint = (TextView)findViewById(R.id.tvComplaint);
        tvCompNo = (TextView)findViewById(R.id.tvCompNo);
        buttonPrint = (Button)findViewById(R.id.buttonPrint);
        linearLayout = (LinearLayout)findViewById(R.id.linearLayout) ;


        Intent intent = getIntent();
        name = intent.getStringExtra("NAME");
        currentDate = intent.getStringExtra("DATE");
        emailid = intent.getStringExtra("EMAIL");
        phone = intent.getStringExtra("MOBILE");
        address = intent.getStringExtra("ADDRESS");
        complaint_no= intent.getStringExtra("COMPNO");
        complaint = intent.getStringExtra("COMPLAINT");
        subject = intent.getStringExtra("SUBJECT");

        tvName.setText(name);
        tvDate.setText(currentDate);
        tvEmail.setText(emailid);
        tvCompNo.setText(complaint_no);
        tvComplaint.setText(complaint);
        tvAddress.setText(address);
        tvSubject.setText(subject);
        tvMobile.setText(phone);
        buttonPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadQRCode();
            }
        });
    }

    public void downloadQRCode() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

        } else {

            PrintAttributes printAttributes = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                printAttributes = (new PrintAttributes.Builder())
                        .setMediaSize(PrintAttributes.MediaSize.ISO_A2)
                        .setMinMargins(PrintAttributes.Margins.NO_MARGINS)
                        .setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME)
                        .build();


                PrintedPdfDocument document = null;

                document = new PrintedPdfDocument(this, printAttributes);

                // start a page
                PdfDocument.Page page = null;

                page = document.startPage(1);

                // draw something on the page
//            linearLayout.getImageMatrix().setScale(1.1f, 1.1f);
                View content = linearLayout;
//                tv1.setTextScaleX(0.5f);
//                tv2.setTextScaleX(0.5f);
//                compno.setTextScaleX(0.5f);
                content.draw(page.getCanvas());
//
//                tv1.setTextScaleX(1f);
//                tv2.setTextScaleX(0.5f);
//                compno.setTextScaleX(0.5f);
                // finish the page

                document.finishPage(page);


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                String filePath = file.getAbsolutePath();
                File pdfFile = new File(filePath, System.currentTimeMillis()+"OnlineFIR.pdf");
                try {
                    FileOutputStream fos = new FileOutputStream(pdfFile);
                    document.writeTo(fos);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //close the document
                document.close();

                Intent myIntent = new Intent(Intent.ACTION_VIEW);
                myIntent.setData(Uri.fromFile(pdfFile));
                Intent j = Intent.createChooser(myIntent, "Choose an application to open with:");
                startActivity(j);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        downloadQRCode();
    }

}
