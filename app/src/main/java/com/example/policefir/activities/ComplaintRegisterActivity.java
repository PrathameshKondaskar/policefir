package com.example.policefir.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.policefir.Model.ComplaintInfoModel;
import com.example.policefir.R;
import com.example.policefir.SharedPreferenceManager;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class ComplaintRegisterActivity extends AppCompatActivity {

    EditText editTextRname,editTextRphone,editTextRaddr,editTextRcomplaint;
    Button buttonCancel1,buttonSubmit;
    LinearLayout linearLayout;
    Spinner category;
    TextView date;
    private ImageView mImageView;
    private Button mButtonChooseImage;

    int mYear,mMonth,mDay;
    String currentDate;
    String complaint_no,complaint;
    static ArrayList<String> categories = new ArrayList<>();

    static ArrayList<String> s = new ArrayList<>();
    static ArrayList<Integer> i = new ArrayList<>();

    Bitmap bm;
    int flag =0;
    String imgurl;
    private static final int PICK_IMAGE_REQUEST =1;
    private static String ALPHA_NUMERIC_STRING="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private Uri mImageUri;
    static String emailid;
    ArrayAdapter<String> ad;

    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    private StorageReference mStorageRef;
    private StorageTask mUploadTask;

    private ProgressDialog progressDialog;

    SharedPreferenceManager sharedPreferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferenceManager = new SharedPreferenceManager(this);

        progressDialog = new ProgressDialog(this);
        linearLayout=(LinearLayout)findViewById(R.id.linearLayout);
        mButtonChooseImage=(Button)findViewById(R.id.openimg);
        mImageView=(ImageView)findViewById(R.id.img);
        category = (Spinner)findViewById(R.id.rspinner);
        editTextRname=(EditText)findViewById(R.id.rname);
        editTextRphone=(EditText)findViewById(R.id.rphone);
        editTextRaddr=(EditText)findViewById(R.id.raddress);
        editTextRcomplaint=(EditText)findViewById(R.id.rcomplaint);
        buttonSubmit=(Button)findViewById(R.id.submit);
        buttonCancel1=(Button)findViewById(R.id.cancel1);
        date=(TextView)findViewById(R.id.date);

        //Firebase Initialize
        mFirestore = FirebaseFirestore.getInstance();
        mAuth= FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mStorageRef= FirebaseStorage.getInstance().getReference();


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        Log.d("year", String.valueOf(mYear));
        mMonth = c.get(Calendar.MONTH);
        Log.d("month", String.valueOf(mMonth));
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.d("day", String.valueOf(mDay));


        date.setText(new StringBuilder().append(mDay).append("-")
                .append(mMonth + 1).append("-")
                .append(mYear).append(" ")+"");
        currentDate=date.getText().toString();

        buttonCancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ComplaintRegisterActivity.this,MainActivity.class));
            }
        });
        categories.add("Others");
        categories.add("Rape");
        categories.add("Murder");
        categories.add("Hit and Run");
        categories.add("Half Murder");
        categories.add("Fraud");
        categories.add("Abduction");
        categories.add("Black Marketing");
        categories.add("Terrorism");
        categories.add("Money Laundering");
        categories.add("Kidnapping");
        categories.add("Extoration");
        categories.add("Child Labour");
        categories.add("Robbery");

        ad = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,categories);
        category.setAdapter(ad);


        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();


            }
        });
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserInfo();
            }
        });
    }


    private void saveUserInfo() {


        complaint_no = randomnumber();
        ViewComplaintNoActivity.cno = complaint_no;

        final String name = editTextRname.getText().toString().trim();
        final String address = editTextRaddr.getText().toString().trim();
        final String phone = editTextRphone.getText().toString().trim();
        complaint = editTextRcomplaint.getText().toString().trim();
        final String subject = category.getSelectedItem().toString();
        emailid = sharedPreferenceManager.getEmail();


        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (name.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextRname;
            editTextRname.setError("name is a required field");
        }
        if (address.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextRaddr;
            editTextRaddr.setError("address is a required field");
        }

        if (phone.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextRphone;
            editTextRphone.setError("mobile no. is a required field");
        }
        if (!phone.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextRphone;
            editTextRphone.setError("mobile no. is a invalid");

        }
        if (phone.length() != 10 || phone.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextRphone;
            editTextRphone.setError("mobile no. must be of 10 digit");
        }
        if(flag==0){
            Toast.makeText(this, "Image Not Selected", Toast.LENGTH_SHORT).show();
        }

        else if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {

            progressDialog.setMessage("Registering complaint...");
            progressDialog.show();
            progressDialog.setCancelable(false);



            final String path = System.currentTimeMillis()+"."+getExtenssion(mImageUri);
            final StorageReference fileRef=mStorageRef.child(path);

//            fileRef.putFile(mImageUri)
//                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
//
//
//
//
//                             imgurl=taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
//
//                            ComplaintInfoModel complaintInfoModel = new ComplaintInfoModel(name,emailid,address,phone,complaint,subject,complaint_no,imgurl,currentDate,user.getUid());
//
//                            mFirestore.collection("Complaints").add(complaintInfoModel)
//                                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
//                                        @Override
//                                        public void onComplete(@NonNull Task<DocumentReference> task) {
//                                            if(task.isSuccessful())
//                                            {
//                                                progressDialog.dismiss();
//                                                Toast.makeText(ComplaintRegisterActivity.this, "Data added", Toast.LENGTH_SHORT).show();
//                                            }
//                                            else
//                                            {
//                                                Toast.makeText(ComplaintRegisterActivity.this, "Data not addded", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//                                    });
//
//                        }
//                    })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//
//                        }
//                    })
//                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//
//
//                        }
//                    });


            fileRef.putFile(mImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return fileRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        Uri downUri = task.getResult();
                        Log.d("PATH", "onComplete: Url: "+ downUri.toString());


                            ComplaintInfoModel complaintInfoModel = new ComplaintInfoModel(name,emailid,address,phone,complaint,subject,complaint_no,downUri.toString(),currentDate,user.getUid());

                            mFirestore.collection("Complaints").add(complaintInfoModel)
                                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentReference> task) {
                                            if(task.isSuccessful())
                                            {
                                                progressDialog.dismiss();
                                              //  Toast.makeText(ComplaintRegisterActivity.this, "Data added", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(ComplaintRegisterActivity.this,ViewComplaintNoActivity.class);
                                                intent.putExtra("NAME",name);
                                                intent.putExtra("DATE",currentDate);
                                                intent.putExtra("EMAIL",emailid);
                                                intent.putExtra("MOBILE",phone);
                                                intent.putExtra("ADDRESS",address);
                                                intent.putExtra("SUBJECT",subject);
                                                intent.putExtra("COMPLAINT",complaint);
                                                intent.putExtra("COMPNO",complaint_no);
                                                startActivity(intent);
                                                finish();
                                            }
                                            else
                                            {
                                                Toast.makeText(ComplaintRegisterActivity.this, "Data not addded", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                    }
                }
            });


        }

    }

    private void openFileChooser()
    {
        linearLayout.setVisibility(View.VISIBLE);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==PICK_IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {
            mImageUri=data.getData();
            Log.d("path",mImageUri.toString());
            mImageView.setImageURI(mImageUri);
            //Picasso.with(this).load(mImageUri).into(mImageView);
            //Bitmap bitmap = (Bitmap)data.getExtras().get(data.getData().toString());
            //mImageView.setImageBitmap(bitmap);
            mImageView.buildDrawingCache();
            flag=1;

        }
    }

    private String getExtenssion(Uri uri)
    {
        ContentResolver cr =getContentResolver();
        MimeTypeMap mime=MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

    }

    public static String randomnumber(){
        i.add(1);
        i.add(2);
        i.add(3);
        i.add(4);
        i.add(5);
        i.add(6);
        i.add(7);
        i.add(8);
        i.add(9);
        i.add(0);
        String val="";
        String[] var = ALPHA_NUMERIC_STRING.split("");
        for(String x : var){
            s.add(x);
        }
        for(int h=0;h<20;h++){
            val="";
            Collections.shuffle(s);
            Collections.shuffle(i);

            for(int j=0;j<4;j++){
                val+=s.get(j);
            }
            val+="-";
            for(int j=0;j<4;j++){
                val+=i.get(j);
            }
            Collections.shuffle(s);
            Collections.shuffle(i);
            val+="-";
            for(int j=0;j<4;j++){
                val+=s.get(j);
            }
            val+="-";
            for(int j=0;j<4;j++){
                val+=i.get(j);
            }

            break;
        }
        return val;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
//                Intent intent = new Intent(this, HomeActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
