package com.example.policefir.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.pdf.PrintedPdfDocument;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.policefir.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ViewComplaintNoActivity extends AppCompatActivity {

    static String cno;
    TextView compno, tv1, tv2;
    Button b;
    LinearLayout linearLayout;
    String currentDate,name,emailid,phone,address,subject,complaint,complaint_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_complaint_no);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        tv1 = (TextView) findViewById(R.id.textView5);
        tv2 = (TextView) findViewById(R.id.textView6);

        compno = (TextView) findViewById(R.id.compno);
        compno.setText(cno);


        Intent intent = getIntent();
        name = intent.getStringExtra("NAME");
        currentDate = intent.getStringExtra("DATE");
        emailid = intent.getStringExtra("EMAIL");
        phone = intent.getStringExtra("MOBILE");
        address = intent.getStringExtra("ADDRESS");
        complaint_no= intent.getStringExtra("COMPNO");
        complaint = intent.getStringExtra("COMPLAINT");
        subject = intent.getStringExtra("SUBJECT");




        b = (Button) findViewById(R.id.btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ViewComplaintNoActivity.this,PDFMakingActivity.class);
                intent.putExtra("NAME",name);
                intent.putExtra("DATE",currentDate);
                intent.putExtra("EMAIL",emailid);
                intent.putExtra("MOBILE",phone);
                intent.putExtra("ADDRESS",address);
                intent.putExtra("SUBJECT",subject);
                intent.putExtra("COMPLAINT",complaint);
                intent.putExtra("COMPNO",complaint_no);
                startActivity(intent);
                finish();


//                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//                        startActivity(intent);
//                        finish();


            }
        });


    }
//    @Override
//    public void onBackPressed() {
//
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(ViewComplaintNoActivity.this);
//
//        builder.setTitle("Logout");
//        builder.setMessage("Do you want to Logout ?");
//
//        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
//                startActivity(intent);
//                finish();
//                dialog.dismiss();
//
//            }
//        });
//
//        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        AlertDialog alert = builder.create();
//        alert.show();
//
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
//                Intent intent = new Intent(this, HomeActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

   }
