package com.example.policefir.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.policefir.Model.ComplaintInfoModel;
import com.example.policefir.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class DetailComplaintActivity extends AppCompatActivity {

    TextView tvDate,tvName,tvEmail,tvPhone,tvAddress,tvSubject,tvComplaint,tvImage,tvComplaintNo;
    ImageView imageView;
    ArrayList<? extends ComplaintInfoModel> complaintInfoModelList;
    String date,name,email,phone,address,subject,complaint,complaintNo,imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_complaint);
        imageView = (ImageView)findViewById(R.id.img);
        tvDate=(TextView)findViewById(R.id.t0);
        tvName=(TextView)findViewById(R.id.t1);
        tvEmail=(TextView)findViewById(R.id.t2);
        tvPhone=(TextView)findViewById(R.id.t3);
        tvAddress=(TextView)findViewById(R.id.t4);
        tvSubject=(TextView)findViewById(R.id.t5);
        tvComplaint=(TextView)findViewById(R.id.t6);
        tvComplaintNo=(TextView)findViewById(R.id.t7);

        Intent intent = getIntent();
        tvDate.setText(intent.getStringExtra("Date"));
        tvName.setText(intent.getStringExtra("Name"));
        tvComplaintNo.setText(intent.getStringExtra("ComplaintNo"));
        tvComplaint.setText(intent.getStringExtra("Complaint"));
        tvSubject.setText(intent.getStringExtra("Subject"));
        tvAddress.setText(intent.getStringExtra("Address"));
        tvPhone.setText(intent.getStringExtra("Phone"));
        tvEmail.setText(intent.getStringExtra("Email"));
        imageUrl=intent.getStringExtra("ImageUrl");

// ImageView in your Activity
       // ImageView imageView = findViewById(R.id.imageView);

// Download directly from StorageReference using Glide
// (See MyAppGlideModule for Loader registration)
        Glide.with(this /* context */)
                .load(imageUrl)
                .into(imageView);
    }
}
