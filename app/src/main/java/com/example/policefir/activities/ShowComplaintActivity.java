package com.example.policefir.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.policefir.Model.ComplaintInfoModel;
import com.example.policefir.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class ShowComplaintActivity extends AppCompatActivity {

    ListView listView ;
    ProgressBar progressBar;
    ArrayList<ComplaintInfoModel> complaintInfoModelList;
    //Firebase
    FirebaseFirestore mFireStore;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userId;
    public static String CHECK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_complaint);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView)findViewById(R.id.listView);
        progressBar = (ProgressBar)findViewById(R.id.progress_circle);

        mFireStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();


        Intent intent = getIntent();

        String check = intent.getStringExtra(CHECK);
        if(check.matches("user")) {
            userId = user.getUid();
            getUserDataFromFirebase();
        }else
        {
            getAdminDataFromFirebase();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ShowComplaintActivity.this,DetailComplaintActivity.class);
                intent.putExtra("Date",complaintInfoModelList.get(position).getCurrentDate());
                intent.putExtra("Name",complaintInfoModelList.get(position).getName());
                intent.putExtra("Email",complaintInfoModelList.get(position).getEmail());
                intent.putExtra("Phone",complaintInfoModelList.get(position).getPhone());
                intent.putExtra("Address",complaintInfoModelList.get(position).getAddress());
                intent.putExtra("Subject",complaintInfoModelList.get(position).getSubject());
                intent.putExtra("Complaint",complaintInfoModelList.get(position).getComplaint());
                intent.putExtra("ComplaintNo",complaintInfoModelList.get(position).getComplaint_no());
                intent.putExtra("ImageUrl",complaintInfoModelList.get(position).getImageUrl());

                startActivity(intent);
            }
        });

    }

    private void getUserDataFromFirebase() {

        mFireStore.collection("Complaints")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {
                            progressBar.setVisibility(View.GONE);
                            complaintInfoModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if (userId.matches(document.getData().get("userId").toString())) {

                                    ComplaintInfoModel complaintInfoModel = document.toObject(ComplaintInfoModel.class);
                                    complaintInfoModelList.add(complaintInfoModel);

                                }
                            }

                            CustomAdapter customAdapter = new CustomAdapter(ShowComplaintActivity.this,android.R.layout.simple_list_item_1,complaintInfoModelList);
                            listView.setAdapter(customAdapter);
                        }
                    }
                });

    }

    private void getAdminDataFromFirebase() {

        mFireStore.collection("Complaints")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {
                            progressBar.setVisibility(View.GONE);
                            complaintInfoModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {



                                    ComplaintInfoModel complaintInfoModel = document.toObject(ComplaintInfoModel.class);
                                    complaintInfoModelList.add(complaintInfoModel);


                            }

                            CustomAdapter customAdapter = new CustomAdapter(ShowComplaintActivity.this,android.R.layout.simple_list_item_1,complaintInfoModelList);
                            listView.setAdapter(customAdapter);
                        }
                    }
                });

    }

    public class CustomAdapter extends ArrayAdapter<ComplaintInfoModel>
    {
        ArrayList<ComplaintInfoModel> complaintInfoModelList;
        Context context;
        View view;
        public CustomAdapter(Context context, int resource, ArrayList<ComplaintInfoModel> complaintInfoModelList) {
            super(context, resource, complaintInfoModelList);
            this.context = context;
            this.complaintInfoModelList = complaintInfoModelList;
        }


        @Override
        public View getView(int position,View convertView,ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_show_complaint, parent, false);

            TextView tvUsername = (TextView)view.findViewById(R.id.username);
            TextView tvComplaintNo = (TextView)view.findViewById(R.id.complaintno);
            TextView tvSubject = (TextView)view.findViewById(R.id.subject);
            tvUsername.setText(complaintInfoModelList.get(position).getName());
            tvComplaintNo.setText(complaintInfoModelList.get(position).getComplaint_no());
            tvSubject.setText(complaintInfoModelList.get(position).getSubject());


            return  view;
        }
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
//                Intent intent = new Intent(this, HomeActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }






}
