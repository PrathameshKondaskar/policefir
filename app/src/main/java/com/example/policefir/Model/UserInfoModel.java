package com.example.policefir.Model;

public class UserInfoModel {
    public final static String FIRESTORE_COLLECTION_USERINFO = "UserInfo";
    String fullName, email,password,mobileNo,userId;

    public UserInfoModel(String fullName, String email, String password, String mobileNo, String userId) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.mobileNo = mobileNo;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


}
