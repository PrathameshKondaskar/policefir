package com.example.policefir.Model;

public class EmergencyContactModel {
    public final static String FIRESTORE_COLLECTION_EMERGENCY_CONTACT = "emergencyContact";
    String name,number,userId;

    public EmergencyContactModel() {
    }

    public EmergencyContactModel(String name, String number, String userId) {
        this.name = name;
        this.number = number;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
