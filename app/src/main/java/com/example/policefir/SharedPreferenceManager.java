package com.example.policefir;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {
    private final String FILENAME = "policefir";
    private final String KEY_EMAIL = "email";

    private SharedPreferences sharedPreferences;

    public SharedPreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
    }

    private void putValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setEmail(String email) {
        putValue(KEY_EMAIL, email);
    }

    public String getEmail() {
        return sharedPreferences.getString(KEY_EMAIL, null);
    }

    public void clearSharedPreferences() {
        setEmail(null);
    }
}
